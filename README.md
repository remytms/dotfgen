[![pipeline status](https://gitlab.com/remytms/dotfgen/badges/main/pipeline.svg)](https://gitlab.com/remytms/dotfgen/commits/main)

dotfgen
=======

dotfgen is a cli program that generates files based on a template file.
It lets you anonymise your configuration files before sharing it with
the rest of the world.

See help for more info.


Installation
------------

```shell
pipx install dotfgen
```
