# SPDX-FileCopyrightText: 2019 Rémy Taymans <remytms@tsmail.eu>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Executed when module is run as a script."""

from dotfgen import cli

cli.main()  # pylint: disable=no-value-for-parameter
