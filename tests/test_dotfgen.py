# SPDX-FileCopyrightText: 2019 Rémy Taymans <remytms@tsmail.eu>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""Test file"""

import tempfile
from pathlib import Path

from dotfgen import cli


def test_complete_tpls():
    """
    Test for the complete method.
    """
    with tempfile.TemporaryDirectory("dotfgen-test") as tmpdir:
        # Populate testing dir
        ptmpdir = Path(tmpdir)
        f = Path(ptmpdir / "test")
        f.touch()
        t = Path(ptmpdir / "test.dftpl")
        t.touch()
        of = Path(ptmpdir / "other_file")
        of.touch()
        od = Path(ptmpdir / "other_dir")
        od.mkdir()
        cf_d = Path(ptmpdir / "config_dir")
        cf_d.mkdir()
        cf_f = Path(ptmpdir / "config_dir" / "sub_file")
        cf_f.touch()
        cf_t = Path(ptmpdir / "config_dir" / "sub.dftpl")
        cf_t.touch()
        cf_sd = Path(ptmpdir / "config_dir" / "sub_conf_dir")
        cf_sd.mkdir()
        cf_sd_f = Path(ptmpdir / "config_dir" / "sub_conf_dir" / "subsub")
        cf_sd_f.touch()
        cf_sd_t = Path(
            ptmpdir / "config_dir" / "sub_conf_dir" / "subsub.dftpl"
        )
        cf_sd_t.touch()

        # Testing not recursive
        comp_res = cli.complete_tpls(None, None, str(ptmpdir / "test"))
        assert {str(t)} == set(comp_res)

        comp_res = cli.complete_tpls(None, None, str(ptmpdir / "other"))
        assert {str(od)} == set(comp_res)

        comp_res = cli.complete_tpls(None, None, str(ptmpdir / "e"))
        assert {str(t), str(od)} == set(comp_res)

        comp_res = cli.complete_tpls(None, None, str(ptmpdir / "dir"))
        assert {str(od), str(cf_d)} == set(comp_res)

        comp_res = cli.complete_tpls(None, None, str(ptmpdir))
        assert {str(t), str(od), str(cf_d)} == set(comp_res)

        # Testing recursive
        comp_res = cli.complete_tpls_recursive(
            None, None, str(ptmpdir / "sub")
        )
        assert {str(cf_t), str(cf_sd_t)} == set(comp_res)

        comp_res = cli.complete_tpls_recursive(None, None, str(ptmpdir))
        assert {str(t), str(cf_t), str(cf_sd_t)} == set(comp_res)
